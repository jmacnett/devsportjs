////////////////////////////////////////////////////////////////////////

// Make sure console.log writes to process stderr and not stdout,
// or debug logs may end up being sent to play agent and interpreted
// as command responses!
console.log = x => process.stderr.write(x + "\n");

// Convert a standard azimuth rotation value to an x/y vector.
function azimuth(r) {
	r = Math.PI * 2 * r;
	return {
		x: Math.sin(r),
		y: -Math.cos(r)
	};
}

// Main tank AI body, receives incoming message object including
// game state and self-identification, and returns response object
// to send back to host.
function tankai(msg) {

	// Search the grid for my own tank.
	var myx, myy, mytank;
	for(var x = 0; x < msg.data.size.x; x++)
		for(var y = 0; y < msg.data.size.y; y++) {
			var i = msg.data.grid[y][x];
			if(i && (typeof(i.o) != "undefined")
				&& (msg.data.players[i.o].n == msg.me)) {
				myx = x;
				myy = y;
				mytank = i;
			}
		}

	// If my tank not found, return any minimal message
	// to cause it to spawn if possible.
	if(!mytank)
		return true;

	// AI logic to determine command for my tank follows.  The
	// existing logic is a fairly naive example; it only attempts
	// to examine one space away (touch distance) to determine
	// which actions are valid and likely fruitful, and chooses
	// randomly from among the possibilities with virtually no
	// tactical awareness nor strategy.

	// Helper to add a vector to my current position,
	// with appropriate wrap-around.
	addmod = v => { return {
		x: (myx + v.x + msg.data.size.x) % msg.data.size.x,
		y: (myy + v.y + msg.data.size.y) % msg.data.size.y
	}; };

	// Calculate positions in front, left, and right.
	var fwd = addmod(azimuth(mytank.r));
	var rt = addmod(azimuth(mytank.r + 0.25));
	var lt = addmod(azimuth(mytank.r - 0.25));

	// Construct a set of choices, from which to pick randomly.
	var choices = [];

	// If position in front is open...
	if(!msg.data.grid[fwd.y][fwd.x]) {

		// Add option of moving forward, with heavy weight,
		// since this will move the action quicker.
		choices.push("f");
		choices.push("f");
		choices.push("f");
		choices.push("f");

		// Tank can shoot without risk of backfire, so fire
		// every so often, hoping to score a hit on a tank
		// in front.  It's also possible to determine if it
		// WOULD hit a good target, but the implementation is
		// left as an exercise for the reader.
		choices.push("s");
	}

	// If there's an open path to the left or right, then
	// each of those is an option, respectively.
	if(!msg.data.grid[rt.y][rt.x])
		choices.push("r");
	if(!msg.data.grid[lt.y][lt.x])
		choices.push("l");

	// If ahead, left, and right were all blocked, there will
	// be no choices.  This must mean the tank is stuck in a
	// dead-end; turn either way to expose the way back.
	if(!choices.length) {
		choices.push("r");
		choices.push("l");
	}

	// Return tank's current time and position (required
	// validation fields) and choice of action.
	return {
		t: msg.data.turn,
		x: myx,
		y: myy,
		c: choices[Math.floor(Math.random() * choices.length)]
	};
}

// Process standard input one line at a time.
require("readline")
.createInterface({input: process.stdin})
.on("line", msg => {
	// Incoming message from agent will be a complete JSON
	// document on one line of text; parse it.
	msg = JSON.parse(msg);

	// Run the tank AI against the incoming message to
	// determine outgoing reply.
	var resp = tankai(msg);

	// Respond to he agent, to relay to host, with response
	// message formatted as a JSON document on on line of
	// text, with a newline to terminate it.
	process.stdout.write(JSON.stringify(resp) + "\n");
});

////////////////////////////////////////////////////////////////////////
