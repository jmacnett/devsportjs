========================================================================
                         DevSport Relay Agents
------------------------------------------------------------------------

The relay agents manage the interconnection and message formatting
necessary to play DevSport over the internet.

There are 3 different agents:

- play:
	- Manages player registration and authentication.
	- Wraps around (or is wrapped by) a player's AI code,
	  communicating with it via JSON messages over stdin/stdout
	  pipes.
	- Relays msgpack messages to/from the game host via the hub.

- host:
	- Managed registration and authentication for a game session.
	- Wraps around (or is wrapped by) the game host logic,
	  communicating with it via JSON messages over stdin/stdout
	  pipes.
	- Relays msgpack messages to/from all players/spectators via
	  the hub.

- hub:
	- Provides central identity management and authentication.
	- Provides URL namespace for games.
	- Relays messages between host and players/spectators.

All network traffic is via WebSockets over HTTP or HTTPS, and should be
compatible with any router/firewall.  Only the hub needs to be
accessible from the outside; game hosts and players all make only
outgoing connections to the hub.

------------------------------------------------------------------------
-- PLAY AGENT

To join a game, use the play agent.

To have the play agent wrap and manage your AI, use:
	node play --url=... --user=... --passwd=... --chdir=... --cmd=...

To have your AI wrap around the play agent, it should run:
	node play --url=... --user=... --passwd=... --stdio

--url=...
	The URL provided by game host to join the game, which will be
	in a form like:
		wss://devsport-hub-address/join/gamename

--user=...
	The username you want to use.
		- Only alphanumeric characters are allowed.
		- Length may be limited by the hub.
		- Case sensitive!
		- Usernames are GLOBAL within the hub, across games.

--passwd=...
	Password to use for this username.  The first time you sign in
	with a new username, it will register the account with this
	password.  You will need to use the same password every time you
	sign in later; there is no mechanism to change, reset, or
	recover passwords.  Passwords are protected by a key-stretching
	transform client-side, and hashed hub-side.

--chdir=...
	OPTIONAL: Change the working directory before running the cmd.
	For example, for the built-in sample AI, if you run the play
	agent from within the devsportjs/agent subdirectory:
		--chdir=../player --cmd="node main"

--cmd=...
	A command to wrap.  Either this or --stdio must be specified.
	The play agent will run this command as a subprocess, restart
	it automatically if it exits as long as the play agent is
	running, and relay messages to its stdin/stdout.  All stdout
	traffic is captured and processed as JSON messages to the host,
	so use stderr if you want debug/trace messages.

--stdio
	Indicate that the play agent is to be wrapped by the AI program
	itself; useful if you want to have more control over the process
	lifetime of your AI.  The agent will expect to receive messages
	to relay to the host as newline-terminated JSON on its stdin,
	and will write messages received from the host in the same
	format to its stdout.

------------------------------------------------------------------------
-- HOST AGENT

To host a game, use the host agent.

To have the play agent wrap and manage your game logic, use:
	node host --url=... --auth=... --chdir=... --cmd=...

To have your game logic wrap around the host agent, it should run:
	node host --url=... --auth=... --stdio

--url=...
	The URL at which the game is hosted.  This will be the root URL
	of the hub on which it will be hosted, followed by /host/ and
	then the name of the game, e.g.:
		wss://devsport-hub-address/host/gamename
	Game names are limited to alphanumeric characters only, and the
	hub may impose a length restriction.

--auth=...
	An token/password to validate access to this game.  The first
	time you host a game at this address, it will register the
	game with this credential, and you need to use the same
	credential every time you connect the host at this URL.  There
	is no mechanism to change, reset, or recover auth tokens.  They
	are also protected by a key-stretching transform client-side,
	and are stored hashed hub-side.

--chdir...
	OPTIONAL: Change the working directory before running the cmd.
	For example, for the built-in host logic, if you run the host
	agent from within the devsportjs/agent subdirectory:
		--chdir=../game --cmd="node main"

--cmd=...
	A command to wrap.  Either this or --stdio must be specified.
	See corresponding play agent documentation above for details.

--stdio
	Indicate that the host agent is to be wrapped by the game logic
	process itself.  See corresponding play agent documentation
	above for details.

------------------------------------------------------------------------
-- HUB

The hub is used to host a central communication relay for DevSport
players, hosts, and viewers.  Multiple different games, of different
types, can be managed by a single hub.

Hosting a useful hub is a more advanced deployment than running the
play or host agents, and experience configuring web hosting is
recommended.

The hub stores authentication information for games and players,
validates credentials, and forwards identity information with messages
to the host for players.  All credentials are global within a hub
across all games, so games can rely on consistent usernames across
games for purposes such as whitelisting.

To run a hub, simply run:
	node hub ...

All options are optional.  Some common options:

--host=...:
	Bind to a specific local address.  The default is 127.0.0.1.

--port=...:
	Bind to a specific TCP port.  The default is 8080.

Running the hub behind a reverse proxy providing TLS termination is
strongly recommended to protect both user credentials, and the
authenticity of game state data for all parties.  Serving TLS directly
from the hub agent is not currently supported nor planned.

========================================================================
