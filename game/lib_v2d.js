////////////////////////////////////////////////////////////////////////
// Constructor

function v2d(x, y) {
	this.x = x;
	this.y = y;
};
module.exports = v2d;

////////////////////////////////////////////////////////////////////////
// Alternative Construction

v2d.azimuth = function(r) {
	r = Math.PI * 2 * r;
	return new v2d(Math.sin(r), -Math.cos(r));
}

v2d.z = new v2d(0, 0);
v2d.n = new v2d(0, -1);
v2d.s = new v2d(0, 1);
v2d.e = new v2d(1, 0);
v2d.w = new v2d(-1, 0);

////////////////////////////////////////////////////////////////////////
// Methods

v2d.prototype.toString = function() {
	return "(" + this.x + "," + this.y + ")";
};

v2d.prototype.eq = function(v) {
	return v && (this.x == v.x) && (this.y == v.y);
};

v2d.prototype.add = function(v) {
	return new v2d(this.x + v.x, this.y + v.y);
};

v2d.prototype.sub = function(v) {
	return new v2d(this.x - v.x, this.y - v.y);
};

v2d.prototype.mul = function(s) {
	return new v2d(this.x * s, this.y * s);
};

v2d.prototype.div = function(s) {
	return new v2d(this.x / s, this.y / s);
};

v2d.prototype.neg = function() {
	return new v2d(-this.x, -this.y);
};

v2d.prototype.abs = function() {
	return new v2d(Math.abs(this.x), Math.abs(this.y));
};

v2d.prototype.len = function() {
	return Math.sqrt(this.x * this.x + this.y * this.y);
};

v2d.prototype.mod = function(v) {
	var x = this.x % v.x;
	if((x / v.x) < 0)
		x += v.x
	var y = this.y % v.y;
	if((y / v.y) < 0)
		y += v.y;
	return new v2d(x, y);
};

v2d.prototype.floor = function() {
	return new v2d(Math.floor(this.x), Math.floor(this.y));
};

v2d.prototype.nn = function() {
	return new v2d(Math.floor(this.x + 0.5), Math.floor(this.y + 0.5));
};

v2d.prototype.rot = function(r) {
	r = Math.PI * 2 * r;
	var s = Math.sin(r);
	var c = Math.cos(r);
	return new v2d(this.x * c - this.y * s,
		this.x * s + this.y * c);
};

v2d.addaz = function(r, d) {
	if(typeof(d) == "undefined")
		d = 1;
	return new v2d(this.x + Math.sin(r) * d,
		this.y + Math.cos(r) * d);
};
