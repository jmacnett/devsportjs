const comms = require("./lib_comms");
const config = require("./config");
const gamestate = require("./lib_gamestate");
const rehydrate = require("./lib_rehydrate");
const statedb = require("./lib_statedb");

var state;

function worldtick() {
	if(config.restart && (state.turn >= config.restart))
		state = new gamestate(config);
	else if(config.endturn && (state.turn >= config.endturn))
		return;
	state.turn++;
	state.tick(comms.inbox);
	delete(comms.inbox);
	comms.send(state);
	if(config.save)
		statedb.save(state.turn, state, () => {});
}

statedb.loadlatest(st => {
	state = st
		? rehydrate(gamestate, st)
		: new gamestate(config);
	comms.send(state);
	setInterval(worldtick, config.speed);
});
