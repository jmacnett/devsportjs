const rehydrate = require("./lib_rehydrate");
const v2d = require("./lib_v2d");

function grid(size, grid) {
	this.size = size.abs().floor();
	this.grid = grid || [ ];
};
module.exports = grid;

grid.prototype.get = function(v) {
	v = v.mod(this.size);
	var row = this.grid[v.y];
	return row && row[v.x];
};

grid.prototype.set = function(v, p) {
	v = v.mod(this.size);
	var g = this.grid;
	var row = g[v.y] || [ ];
	g[v.y] = row;
	row[v.x] = p;
};

grid.prototype.scan = function(f) {
	for(var y = 0; y < this.size.y; y++)
		for(var x = 0; x < this.size.x; x++) {
			var v = new v2d(x, y);
			f.call(this, v);
		}
};

grid.prototype.fillfrom = function(f) {
	var self = this;
	this.scan(function(v) { this.set(v, f(v)); });
};

grid.prototype.fill = function(p) {
	this.fillfrom(function() { return p; });
};

grid.prototype.rehydrated = function(from) {
	this.size = rehydrate(v2d, this.size);
};
