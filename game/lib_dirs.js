const v2d = require("./lib_v2d");

var d8 = {
	nw: new v2d(-1, -1),
	n: new v2d(0, -1),
	ne: new v2d(1, -1),
	sw: new v2d(-1, 1),
	s: new v2d(0, 1),
	se: new v2d(1, 1),
	e: new v2d(1, 0),
	w: new v2d(-1, 0)
};

d8.n.r45 = d8.ne;
d8.ne.r45 = d8.e;
d8.e.r45 = d8.se;
d8.se.r45 = d8.s;
d8.s.r45 = d8.sw;
d8.sw.r45 = d8.w;
d8.w.r45 = d8.nw;
d8.nw.r45 = d8.n;
for(var n in d8) {
	var v = d8[n];
	v.n = n;
	v.r45.l45 = v;
	v.r90 = v.r45.r45;
	v.r90.l90 = v;
}
for(var n in d8) {
	var v = d8[n];
	v.r135 = v.r90.r45;
	v.r135.l135 = v;
}
for(var n in d8)
	d8[n].o = d8[n].r90.r90;

var d4 = {
	n: d8.n,
	s: d8.s,
	e: d8.e,
	w: d8.w
};

module.exports = { d4: d4, d8: d8 };
for(var k in d8)
	module.exports[k] = d8[k];
