const child_process = require("child_process");
const connect = require("connect");
const path = require("path");
const readline = require("readline");
const static = require("serve-static");

function startsub(name, cmd) {
	var proc = child_process.spawn(cmd, {
		shell: true,
		stdio: "pipe pipe pipe".split(" ")
	});
	proc.on("close", (code, signal) => {
		console.log(name + " exit: " + JSON.stringify({
			code: code,
			signal: signal
		}));
		process.exit(1);
	});
	proc.on("error", err => {
		console.log(name + " error: " + err);
		process.exit(1);
	});
	function line(l) { console.log(name + ": " + l); }
	readline.createInterface({input: proc.stdout}).on("line", line);
	readline.createInterface({input: proc.stderr}).on("line", line);
}

const viewport = 49280;
const hubport = 49281;

process.chdir(".." + path.sep + "agent");
startsub("hub", "node hub --port=" + hubport);
setTimeout(() => {
		startsub("host", "node host --url=ws://127.0.0.1:"
		+ hubport + "/host/test --auth=test123 --cmd=\"cd .."
		+ path.sep + "game && node main\"");
}, 500);
setTimeout(() => {
	for(var i = 0; i < 8; i++)
		startsub("p" + i, "node play --url=ws://127.0.0.1:"
			+ hubport + "/join/test --user=test" + i
			+ " --passwd=test" + i + " --cmd=\"cd .." + path.sep
			+ "player && node main\"");
}, 2000);

connect().use(static(".." + path.sep + "viewer"))
	.listen(viewport, () => {});
