------------------------------------------------------------------------

This is a wrapper around the hub, host and play agents, and a static web
server for the viewer, for testing devsportjs against the local git repo
without a central host.

Just run "node main" to start it.

- Make sure dependencies are setup correctly first.
- The hub will be run on port 49281, and the viewer on port 49280.
- A game called "test" will be registered.
- The viewer can be accessed at:

	http://localhost:49280/#ws://localhost:49281/join/test

------------------------------------------------------------------------
